# Branching (if clause)
# code block implies indententaion
# 4 spaces  indentation 
# True and False with first letter on capital 

if False:
	print("this is true")
elif True & True:
	print("this is false")
else:
	print("is false")
# True and False with first letter on capital 

# While  block 
print("I m  printing hello 3 times")
count = 1
while count<4:
	print("hello x",count)
	count = count +1 

# or for loop
# range creates a sequence of objets from 0 to  n-1
for count in range(4):
	print(count) 

# also we can specify a the start, end and step
for count in range(1000,2000,100):
	print(count) 

# The brake statement 
condition = True
while condition:
	ans = input("Say my name...")
	if (ans == "lalo"):
		break
