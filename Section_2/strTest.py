# This is a string 
hi  = "hello there" 
bere = " my name is Bere"

# We can concantenate strings using +
print(hi+bere)

# We can operate in strings
print("I'm " + " very "*3 + "happy")

# And put an integer directly
# blanks are directly inserted in the ouput 
age = 31
print("I'm",age, "years old.");

# we can ask for an input
ans = input("I like ... ")
print("Yes ! I like",ans)

# I can cast th input 
ans = int(input("I many years I'm :"))
print("No! I have",ans-5 )

# we can compare strings?
one = "one"
two = "two" 
three = "three"

# Comparing string content if same content 
print ("one == two",one==two)

print ("three > one ",three > one)
