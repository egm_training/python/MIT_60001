## Section 6

### Recursion

Steps

- Identify the recursion structure
- Identify the base case
- Reduce the problem to a smaller version of itself

Python creates local frames for each function. There is no problem with local variables.

```mermaid
graph LR
    subgraph f scope;   1;  n1;  end
    subgraph f scope;   2;  n2;  end
    subgraph f scope;   3;  n3;  end
    subgraph f scope;   4;  n4;  end
    subgraph G scope;  c1[FG]; c2[code];    end
    c1 --> 4 ;  4  --> 3 ; 3 --> 2 ; 2 -->1
```

Code

```python
def fact(n):
    if n == 1 :
        return 1
    else:
        return n*fact(n-1)
```

#### Hanoi towers

**Algorithm**
Supposing the bigger disk being number $n$ and the stack over it $n-1$

1. Move $n-1$ to the spare tower
1. Move $n$ to the end tower
1. Move $n-1$ to the end tower

Base condition

> if n == 1 then move n to the end tower.

**Code**

```python
def printMove(fr, to):
    print("I moved from ",fr,"to",  to)

def towers(n,fr,to, spare):
    if n == 1:
        printMove(fr,to)
    else:
        towers(n-1,fr,spare,to)
        towers(1,fr,to,spare)
        towers(n-1,spare,to,fr)

towers(4,'start','end','spare')
```

#### Fibonacci

A case with two base cases:

```python
def fib(x):
    if x==0 or x==1:
        return 1essivity
    else:
        return fib(x-1) + fib(x-2)

print(fib(2))
```

## Dictionaries

Combine a key (not necessarily an int) with a record. To use for data structures.

> The **KEY** is an **immutable** type

| Key   | Value   |
| ----- | ------- |
| key 1 | Value 1 |
| key 2 | Value 2 |
| key 3 | Value 3 |

An Empty dictonary

```python
my_dict = {}
```

**Initialize**

```python
grades = { "Birus":10 , "Lalo":0}
```

**Indexing**

```python
my_dict[key]
```

**Add**

```python
my_dict[key] = something
```

**Test or look for a key**

```python
key in my_dict
```

**delete**

```python
del(my_dict[key])
```

**Iterator**

An Iterator returns the key :

```python
for key in my_dict
print(key,"grade =",my_dict[key])
```

**Get Key and values**

```python
keys  =  grades.keys()    # Immutable array
val   =  grades.values()
for item in val:
    print("grade =", item)
```

**Example**

> Dictionaries are highly efficient to store information → optimize access.

Example : Fibonacci

```python
from timeit import default_timer as timer

def fibodic(x,d):
    if x in d:
        return d[x]
    else:
        ans =  fibodic(x-1,d) + fibodic(x-2,d)
        d[x] = ans
        return ans

d={1:1, 2:2}
start = timer()
res = fibodic(1000,d)
end = timer()
print("sol = ",res,"in",end-start,"s")
```

Time for x = 42 not using dictionaries = **92.66 s**
Time for x = 42 using dictionaries = **0.0000995 s**
Time for x = 1000 using dictionaries = **0.00097 s**


**tags**: #python #recursion