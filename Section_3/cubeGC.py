# Check and guess algo
# Search for the cube of one number
import numpy as np
number = float(input("Tape a number: "))
eps = 1.0e-3
step = 1.0e-5

for guess in np.arange(0,abs(number),step):
	if abs(guess**3 - number)<eps:
		break

print("the guess is: ", guess)
