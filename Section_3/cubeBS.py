# Bisection search
# Search for the cube of one number
import numpy as np
number = float(input("Tape a number: "))
eps = 1.0e-3
low = 0.0
top = abs(number)
guess = (top+low)/2.0
count = 1

while abs(guess**3-abs(number)) >= eps:
	if guess**3 > abs(number):
		top = guess
	else:
		low = guess
	guess =  (top+low)/2.0
	count += 1

if number < 0:
	guess = -1.0*guess

print("the guess is: ", guess)
print("number of guesses",count)
