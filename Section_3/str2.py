#Some other fucntions with strings

s = "Hello, World!"

print(s,'length is',len(s))

# we see a string as an array of chars 
# start at 0
for i in range(len(s)):
	print(s[i])

# we can use slide
print(s[:4])
print(s[4:])
print(s[::-1])
print(s[4:8])

# string are inmutable,
# we can change it
# we need to use another string objet from the current using sliding 
print('y'+s[1:])

#we can use a loop on the range of the string or any other array
for var in s:
	print(var)

# Var is a string .. and stay in the scope after defined, weird
print(type(var))

# Comparing common letters 
a = input("type a word:\n ")
b = input("type a second word:\n ")
res ="";

for var in b:
	if var in a:
		res = res+var

print ("common letters are: ",res)
