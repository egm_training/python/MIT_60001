# A function can retrun nothing 
def f_a():
	print("Hello from function A")
	# return a varianle none

# A function can return a variable
def f_b(y):
	print("Hello from function B")
	return y;

# We can pass a function 
def f_c(f_int, *args):
	print("Hello from function C")
	return f_int(*args)

# print the adress of fuction objet A
print ( f_a )

# print the result of the call of function A
print (f_a())

# print the adress of the call of function B
print(f_b)

# print the result of the call of function B
print ( f_b(1) )

# print the adress of the  function object C
print (f_c)

# print the result of the call of function C
print ( f_c(f_a) )

# print the adress of the  function object C
print (f_c)

# print the result of the call of function C
print ( f_c(f_b,42) )
