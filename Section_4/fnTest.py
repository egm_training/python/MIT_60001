# Starting to use functions & structuring the code
# Decomposition: divide the code into modules 
# Achieve abstraction with specifications and docstring

#key name   input
def is_even(i):
	# docstring
	"""
	Input : i, a positive int
	Returns True if is even, otherwise false
	"""
	# Body
	print("inside is_even")
	return i%2 == 0

number = int(input("Give a number: "))
print("Number ",number, "is even?",is_even(number))
