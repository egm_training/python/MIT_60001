# we define a function using a variable not 
# defined in the scope

def fdummy1():
	y = x * 42
	print(y)

def fdummy2():
	x += 1 ;  'THIS IS NOT ALLOWED'
	y = x * 42
	print(y)

def fdummy3():
	x  = 6 ;  'THIS IS ALLOWED'
	y = x * 7
	print(y)


# X is defined in the global scope of this script and can be
# be used by all the fucntion calls.
# acts like a global CONSTANT  variable in the scope
x = 1;
fdummy1()
fdummy3()

print(x)
