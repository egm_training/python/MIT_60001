# Tuples are collections of different objets
# You can modify this variables 
# represented wirth parentheses

# empty tuple 
te = ()

# tupple of different values
tn = ("hello") 
print(type(tn)) ; '<-This is not a tupple is a string'

t0 = ("hello",)
print(type(t0)) ; '<-This is a tupple'

t1 = ("hello", 1.0 , True)
print(type(t1))  

print("Printing the types of the tupple t1")
for i in range(len(t1)):
	print(type(t1[i]))

print("Printing the values of the tupple t1")
for i in range(len(t1)):
	print(t1[i])

# We can concantenate
te = te +("ab",) + (1,"c") + ('d',)
print(te)

 
# Tupples can be used to swap values 
coord = (84 , 19)
print ("coord = ", coord)

coord = (coord[1], coord[0])
print ("coord = ", coord)

x = 84 
y = 19 
print("x= ",x, "y=",y)

(x,y) = (y,x)
print("x= ",x, "y=",y)

# Also to return several values 
def qr(x,y):
	q = x // y;
	r = x % y;
	return (q,r)

x = float(input("enter x : "))
y = float(input("enter y : "))

res = qr(x,y)

print("q and r  are : ", res)

# We can, iterate using tupples
def get_data(inTupple):
    num = () # an empty tupple
    words = () 
    for t in inTupple:
        num += (t[0],) # we save a new tupple member
        if t[1] not in words:
            words += (t[1],)
    min_n = min(num)
    max_n = max(num)
    unique_words = len(words)
    return (min_n, max_n, unique_words)

data = ((1989,"Bere"),(1989,"Lalo"),(1996,"Diana"),(2001,"Pepe"),(2004,"Pepe"),(2020,"Lalo"))

(min_n, max_n, unique_words) = get_data(data);

print("From",min_n,"to",max_n," we have ",unique_words,"different names")

