# Lists 
# List behaves like tupples but they are mutable

l1 = [] # an empty list
l2 = [1,'a',True, 1.0,(1,'a'),[1,'b']]

for l in l2:
    print(type(l)) 

# We have indexing an slicing
print(l2[-1])
print(l2[1:3])
  
# Abut we can modify the list
l2[0] = l2[0]+1
print(l2[0])
l2[-1][1]= 'c'
print(l2[-1])

# Sum of the list 
l3 = list(range(10))
total = 0.0;
for l in l3:
    total += l
print(total)

# Some other operations 
# Append elements 
l3.append(10)
print(l3)

# Concantenante
l4 = list(range(11,20,1))
l5 = l3 + l4
print(l5)

# Extend must be one argument, for exmaple a list 
# or a tupple, is converted to a list
l5.extend([20,21])

# We can delete elements 
del(l5[0])
print(l5)

# delete last element 
l5.pop()
print(l5)

# Or delete an specific objet
l5.remove(10)
print(l5)

# List is useful with strings

s = "This is a test, for strings with lanage = python"
sl = list(s)
print(sl)
print(list(s.split(',')))
