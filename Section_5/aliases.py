#IN python we can uses alises

name = "bere"
apodo = name

aliases = ["bere","berenice"]

for id_o in aliases:
    print(id_o,"with adress:",id(id_o))

print("adding another element to the list")
other = aliases
other.append("birus")

for id_o in aliases:
    print(id_o,"with adress:",id(id_o))


# Cloing a list 
listclone  = other[:]
listclone.append("luz")
print("list has been cloned")
print("This is the new list with adress",id(listclone))
for id_o in listclone:
    print(id_o,"with adress:",id(id_o))

print("This is the old list with adress", id(aliases))
for id_o in aliases: 
    print(id_o,"with adress:",id(id_o))

#We can see that the elements adresses are the same, it 
#only change if we modify the value
print(listclone.sort())
print(sorted(listclone))

#Calling a method from the object regularely mutates the object


