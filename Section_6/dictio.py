# Dictionaires
# Empty
my_dict = {}

# Initialize
grades = { "Birus":10 , "Lalo":0}

# Assign a new value
grades["Cacha"] = 20

# Iterator returns the key
for item in grades:
    print(item,"grade =",grades[item])

# Test if an entry is in the diction
print("Cibou is in grades ?","cibou" in grades )

# delete an entry
print('removing lalo')
del(grades["Lalo"])

# Iterator returns the key
for item in grades:
    print(item,"grade =",grades[item])

keys  =  grades.keys()   
val   =  grades.values()

print("The keys are : ", keys, "and is a", type(keys))
print("The values are : ", val, "and is a", type(val))

for item in val:
    print("grade =", item)