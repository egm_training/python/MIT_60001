from timeit import default_timer as timer 

# Recursion
# Divide and conquer 

# Multiplication example
# Iterative example
def mult_iter(a,b):
    sol = 0
    while (b>0):
        sol += a 
        b-=1
    return sol
start = timer()
t1 = mult_iter(6,7)
end = timer()
print("sol iter",t1,"on",end-start,"s")

# Recursive example
def mult_rec(a,b):
    if (b==1):
        return a
    else :
        return a + mult_rec(a,b-1)
start = timer()
t2 = mult_rec(6,7)
end = timer()
print("sol recu",t2,"on",end-start,"s")

# Factorial example
## Using recursive code
def fact_iter(n):
    sol = 1
    for i in range(1,n+1):
        sol *=i
    return sol
start = timer()
t1 = fact_iter(6)
end = timer()
print("fact iter",t1,"on",end-start,"s")

## Using an iterative approach
def fact(n):
    if n == 1 :
        return 1
    else:
        return n*fact(n-1)

start = timer()
t2 = fact(6)
end = timer()
print("fact recu",t2,"on",end-start,"s")

