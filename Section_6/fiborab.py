from timeit import default_timer as timer 

def fib(x):
    if x==0 or x==1:
        return 1
    else:
        return fib(x-1) + fib(x-2)


start = timer()
res = fib(42)
end = timer()
print("sol = ",res,"in",end-start,"s")