from timeit import default_timer as timer 

def fibodic(x,d):
    if x in d:
        return d[x]
    else:
        ans =  fibodic(x-1,d) + fibodic(x-2,d)
        d[x] = ans 
        return ans

d={1:1, 2:2}

start = timer()
res = fibodic(1000,d)
end = timer()
print("sol = ",res,"in",end-start,"s")